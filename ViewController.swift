//
//  ViewController.swift
//  FireBaseDemo
//
//  Created by Paramvir Singh on 5/19/16.
//  Copyright © 2016 seasia. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController,UIActionSheetDelegate{
    
    @IBOutlet weak var scrollview: UIScrollView!
    
    @IBOutlet weak var txtUsername: UITextField!
    
    @IBOutlet weak var txtFirstname: UITextField!
    
    @IBOutlet weak var txtLastname: UITextField!
    
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var txtPassword: UITextField!
    
    
     var ref = FIRDatabaseReference()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.scrollview.contentInset = UIEdgeInsetsMake(-64, 0, 0, 0);
        
        self.ref = FIRDatabase.database().reference()
        
}
// MARK: Custom Action
    @IBAction func btnClicked(sender: AnyObject) {
        
        if txtEmail.text?.characters.count == 0 || txtUsername.text?.characters.count == 0 || txtFirstname.text?.characters.count == 0 || txtLastname.text?.characters.count == 0 || txtPassword.text?.characters.count == 0 {
            
            let alert = UIAlertController(title: "Alert", message: "Please fill all fileds", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            
            return
        }
        
        if !isValidEmail(txtEmail.text!){
            let alert = UIAlertController(title: "Alert", message: "Invalid email", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }

        
        let dict = [
            "username":txtUsername.text!,
            "firstname":txtFirstname.text!,
            "lastname":txtLastname.text!,
            "password":txtPassword.text!,
            "emailid":txtEmail.text!
            ] as NSMutableDictionary

        
        //Create the AlertController and add Its action like button in Actionsheet
        let actionSheetController: UIAlertController = UIAlertController(title: "Please select", message: "", preferredStyle: .ActionSheet)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
            print("Cancel")
        }
        actionSheetController.addAction(cancelActionButton)
        
        let fireBaseActionButton: UIAlertAction = UIAlertAction(title: "Firebase", style: .Default)
            { action -> Void in
                
                Webservice.sharedInstance.registerUserWithFirebase(dict) { (String) -> Void in
                    
                    if String == "Done" {
                       
                        NSOperationQueue.mainQueue().addOperationWithBlock(){
                            
                             self.showSuccessFullAlert()
                        }
                    }
                    else{
                        NSOperationQueue.mainQueue().addOperationWithBlock(){
                            
                             self.showErrorAlert()
                        }
                        
                    }
                    
                }
                print("fireBaseAction")
        }
        actionSheetController.addAction(fireBaseActionButton)
        
        let ownServerActionButton: UIAlertAction = UIAlertAction(title: "Own Server", style: .Default)
            { action -> Void in
                print("ownServerAction")
                
               
                let url = NSURL(string: "http://templates2.seasiaconsulting.com:9040/MyPeopleAt/Userapi/signupapp")
                
                /*

                  {"email":"fsf@fhf.ghgh","password":"","firstname":"hh","lastname":"kk","dob":"11/11/2000","gender":"female","culture":"sfsf","ethnicity":"sfsf","userType":"email","username":"hhh","imgCode":"","userId":"","DeviceToken":"4646464464","culture_lifestyle":"Musician,Artist","company_name":"seasia"}
                */
                
                let dict1 = [
                    "username":self.txtUsername.text!,
                    "firstname":self.txtFirstname.text!,
                    "lastname":self.txtLastname.text!,
                    "password":self.txtPassword.text!,
                    "email":self.txtEmail.text!,
                    "dob": "11/11/2000",
                    "gender":"male",
                    "culture":"a,b,c",
                    "ethnicity":"a,b,c",
                    "userType":"email",
                    "imgCode":"sjsduyfgodsudshyushu",
                    "userId":"",
                    "DeviceToken":"sgdfsgdfsgdfsgdfsgdfs435634",
                    "culture_lifestyle":"a,v,b,n",
                    "company_name":"demoo"
                    ] as NSMutableDictionary
                
                
                Webservice.sharedInstance.registerUserWithWebservice(url!, body: dict1, completionHandler: { ( data, response, error) -> Void in
                    
                    if (error != nil) {
                        NSOperationQueue.mainQueue().addOperationWithBlock(){
                            
                             self.showErrorAlert()
                            
                        }
                       
                        
                    }else{
                        
                         let responseString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                        print(responseString)
                        
                        NSOperationQueue.mainQueue().addOperationWithBlock(){
                            self.showSuccessFullAlert()
                        }
                        
                    }
                    
                })
        }
        actionSheetController.addAction(ownServerActionButton)
        self.presentViewController(actionSheetController, animated: true, completion: nil)
        

}
    //MARK: Email validation
    
    func isValidEmail(testStr:String) -> Bool {
        print("validate emilId: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    
    //MARK: Textfield delegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        
        if textField == txtUsername {
          txtFirstname.becomeFirstResponder()
        }else if textField == txtFirstname {
          txtLastname.becomeFirstResponder()
        }else if textField == txtLastname {
          txtEmail.becomeFirstResponder()
        }else if textField == txtEmail {
          txtPassword.becomeFirstResponder()
        }else {
          txtPassword.resignFirstResponder()
        }
        
        
        return true
    }
    
    //MARK: Alerts
    func showErrorAlert (){
        let alert = UIAlertController(title: "Alert", message: "Something went wrong", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func showSuccessFullAlert(){
        
        let alert = UIAlertController(title: "Alert", message: "Saved successfully", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
        
        self.txtUsername.text   = ""
        self.txtFirstname.text  = ""
        self.txtLastname.text   = ""
        self.txtPassword.text   = ""
        self.txtEmail.text      = ""
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

